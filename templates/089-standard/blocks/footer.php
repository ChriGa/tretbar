<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="clear-footer" role="contentinfo">
	<div class="fullwidth clear-footer-wrap">		
		
		<div class="innerwidth">
		<?php if ($this->countModules('footnav')) : ?>
			<div class="footnav">
				<div class="module_footer position_footnav">
					<jdoc:include type="modules" name="footnav" style="none" />
				</div>			
			</div>
			<?php endif ?>
		</div>		
	</div>
	<hr style="border-bottom: 5px solid #f8f8f8; margin: 10px auto;" class="innerwidth" />
</footer>
<div id="copyright" class="innerwidth">&copy 2016 finanzierungen, Immobilien, Gastronomiefinanzierungen,  Harald Tretbar - unabhängiger Finanzierungsberater</div>
	
		