<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;

if ($this->countModules('slider')) : ?>
<div class="clear-slider">
	<?php if(!$detect->isMobile() || $detect->isTablet() ) : ?>
		<div id="logoContainer">
			<a href="/" alt="Startseite TRETBAR-Finanzierungen" title="TRETBAR Finanzierungen">
				<div id="logoHeader">
					<h2>Tretbar</h2>
					<h3 id="logoSlogan">Immobilien&amp;Gastronomie<br /><span>Finanzierungen</span></h3>
				</div>
			</a>
			<div class="vcard">
			<p class="tel "><a href="tel:+498999884383">INFO 089 - 9988 4383</a></p>
			<p class="adr"><span class="street-address">Georgenstr. 48</span><br />
				<span class="postal-code">80799 </span><span class="region">M&uumlnchen</span>
			</p>
			</div>
		</div>
			<div class="clear-slider-wrap" id="sliderWrapper">		
				<jdoc:include type="modules" name="slider" style="none" />		
			</div>
	<?php else : ?>
			<div id="logoContainer" class="mobile">
			<a href="/" alt="Startseite TRETBAR-Finanzierungen" title="TRETBAR Finanzierungen">
				<div id="logoHeader">
					<h2>Tretbar</h2>
					<h3 id="logoSlogan">Immobilien&amp;Gastronomie<br /><span>Finanzierungen</span></h3>
				</div>
			</a>
			<div class="vcard">
			<p class="tel"><a href="tel:+498999884383">INFO TELEFON 089 - 9988 4383</a></p>
			<p class="adr"><span class="street-address">Georgenstr. 48 &nbsp;</span>
				<span class="postal-code">80799 </span><span class="region">M&uumlnchen</span>
			</p>
			</div>
		</div>
	<?php endif; ?>
</div>
<?php endif; ?>
