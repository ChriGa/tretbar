<?php
/**
 * @author   	cg@089webdesign.de
 */
 

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
//
?>
<!DOCTYPE html>
<html>
<head>
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
</head>

<body class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. $body_class;
?>">

	<!-- Body -->
		<div class="fullwidth site_wrapper">
			<div id="trenner"></div>
			<?php			
			
			// including header
			//include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');			

			// including slider
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/slider.php');

			// including header
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');

			// including top
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');	

			// including top
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');													
			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
	
			// including bottom
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
			
			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
			
			?>					
			
		</div>
	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
